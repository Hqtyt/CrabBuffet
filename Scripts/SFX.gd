extends AudioStreamPlayer2D

func _ready() -> void:
	var _c = connect("finished", self, "on_finished")

func on_finished():
	queue_free()
