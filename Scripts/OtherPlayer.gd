extends KinematicBody2D

const OTHER_PROJECTILE = preload("res://Scenes/OtherProjectile.tscn")

onready var main: Node = $"../../.."
onready var game: Node2D = $"../.."
onready var player: KinematicBody2D = $"../Player"
onready var projectiles_node: Node2D = $"../../Projectiles"
onready var attack_timer: Timer = $AttackTimer
onready var sfx_fire: AudioStreamPlayer = $SFX_Fire

var speed = 3000.0
var fire_rate = 0.125

var move_input: Vector2 = Vector2.ZERO
var fire_input: Vector2 = Vector2.ZERO


func _ready() -> void:
	speed = player.speed
	fire_rate = player.fire_rate
	attack_timer.wait_time = fire_rate


func _physics_process(delta: float) -> void:
	var _v = move_and_slide(move_input * delta * speed)


func _process(_delta: float) -> void:
	if !main.playing:
		return

	var input = main.get_player_input_state(name)
	move_input = input["move"]
	fire_input = input["fire"]
	var position_state = main.get_player_position_state(name)

	if global_position.distance_to(position_state) >= 2:
		global_position = position_state

	if fire_input != Vector2.ZERO && attack_timer.is_stopped():
		fire()


func fire() -> void:
	sfx_fire.play()

	var new_projectile = OTHER_PROJECTILE.instance()
	new_projectile.player_id_from = name
	new_projectile.modulate = modulate
	new_projectile.damage *= game.player_damage_multiplier
	new_projectile.velocity = fire_input
	projectiles_node.add_child(new_projectile)
	new_projectile.global_position = global_position

	attack_timer.start()


func _on_AttackTimer_timeout() -> void:
	if fire_input != Vector2.ZERO:
		fire()
