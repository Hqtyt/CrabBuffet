extends Area2D


onready var main: Node = $"../../.."
onready var audio: Node = $"../../../Audio"

var speed: float = 250.0
var damage: float = 0.4
var velocity: Vector2
var player_id_from: String

var enemies_to_pierce = 1


func _process(delta: float) -> void:
	global_position += velocity * delta * speed


func _on_Projectile_body_entered(body:Node) -> void:
	if enemies_to_pierce <= 0:
		return
	
	var enemies_node = body.get_parent()

	if enemies_node is Node2D && enemies_node.name == "Enemies":
		enemies_to_pierce -= 1
		modulate.a = 0.5
		body.modulate.a -= damage

		if body.modulate.a > damage:
			audio.play2D("damage", body.global_position)

		if !main.is_singleplayer && !main.is_host: #"other" in name.to_lower():
			if enemies_to_pierce <= 0:
				queue_free()
			return

		if body.modulate.a <= damage:
			main.enemy_killed(player_id_from)
			body.queue_free()

		if enemies_to_pierce <= 0:
			queue_free()
	else:
		queue_free()


func _on_LifeTimer_timeout() -> void:
	queue_free()


func _on_Projectile_area_entered(area:Area2D) -> void:
	_on_Projectile_body_entered(area.get_parent())
