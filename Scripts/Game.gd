extends Node2D

signal wave_start
signal wave_end

const LAG = 0.03

onready var main: Node = $".."
onready var audio: Node = $"../Audio"
onready var enemies: Node2D = $Enemies
onready var enemies_timer: Timer = $Enemies/Timers/EnemiesTimer
onready var players: Node2D = $Players
onready var map: Node2D = $Map
onready var wave_cd_timer: Timer = $WaveCD

var lag_time = -1.0
var wave: int = 0
var player_damage_multiplier: float = 1.0


func _ready() -> void:
	var _c = wave_cd_timer.connect("timeout", self, "start_wave")
	set_process(false)


func start_wave() -> void:
	print("start wave game")
	wave += 1
	player_damage_multiplier = 1.0 - (max(0, wave - 1) / 20.0)

	if !main.is_singleplayer && main.is_host:
		lag_time = LAG
		set_process(true)
	else:
		emit_signal("wave_start")


# From Enemies.gd
func end_wave() -> void:
	emit_signal("wave_end")
	print("WAVE END")
	if wave_cd_timer.is_inside_tree():
		wave_cd_timer.start()


# func update_killed_enemies() -> void:
# 	var death_note = main.get_killed_enemies()

# 	for enemy_name in death_note:
# 		var enemy = enemies.get_node_or_null(enemy_name)
# 		if enemy && !enemy.is_queued_for_deletion():
# 			audio.play2D("pop", enemy.global_position)
# 			enemy.queue_free()
	
# 	killed_enemies = death_note

# func enemy_killed(enemy_name: String) -> void:
# 	if main.is_singleplayer:
# 		return

# 	killed_enemies.append(enemy_name)
# 	main.set_killed_enemies(killed_enemies)


func _process(delta: float) -> void:
	if lag_time <= 0.0:
		set_process(false)

	lag_time -= delta

	if lag_time <= 0.0:
		emit_signal("wave_start")
		set_process(false)


# func get_player_amount() -> int:
# 	return int(max(main.players_id_to_info.keys().size(), players.get_child_count()))
