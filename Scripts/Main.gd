extends Node

onready var playroom: Node = $Playroom
onready var gui: Control = $GUI
onready var game: Node2D = $Game
onready var map: Node2D = $Game/Map
onready var wave_cd_timer: Timer = $Game/WaveCD
onready var player: KinematicBody2D = $Game/Players/Player

var killed_enemies: PoolStringArray = PoolStringArray([])
var water_tiles: PoolStringArray = PoolStringArray([])
var is_singleplayer := true
var is_host := false
var playing := false


func _ready() -> void:
	gui.init_color_picker()
	# var _c = game.connect("wave_start", self, "on_wave_start")
	var _c = game.connect("wave_end", self, "on_wave_end")
	# _c = map.connect("water_spawned", self, "on_water_spawned")
	# _c = map.connect("water_despawned", self, "on_water_despawned")


func join_room(room_code: String = "") -> void:
	playroom.init_playroom(room_code)


func get_room_code():
	return playroom.get_room_code()


func set_player_color(color: Color) -> void:
	player.modulate = color

	if !is_singleplayer:
		playroom.set_player_color(color)


func play_game() -> void:
	print("PLAY GAME")
	if !is_singleplayer && !is_host:
		return

	playing = true
	wave_cd_timer.start()

	if is_host:
		playroom.play_game()


func vec2_to_string(vec2: Vector2, contains_ints: bool = false) -> String:
	var data: String = ""

	if contains_ints:
		data = str(int(vec2.x)) + "," + str(int(vec2.y))
	else:
		data = str(vec2.x) + "," + str(vec2.y)
	
	return data


func string_to_vec2(data: String, contains_ints: bool = false) -> Vector2:
	var vec_data: PoolStringArray = data.split(",") # split_float(",")
	var result: Vector2

	if contains_ints:
		result = Vector2(int(vec_data[0]), int(vec_data[1]))
	else:
		result = Vector2(float(vec_data[0]), float(vec_data[1]))

	return result


func set_player_input_state(move_input: Vector2, fire_input: Vector2) -> void:
	var move_str: String = vec2_to_string(move_input)
	var fire_str: String = vec2_to_string(fire_input)
	var input_state_data: String = move_str + ";" + fire_str
	playroom.set_player_state("me", "input", input_state_data)


func get_player_input_state(player_id: String) -> Dictionary:
	var input_state_data = playroom.get_player_state(player_id, "input")
	if !input_state_data || input_state_data == "":
		return {"move": Vector2.ZERO, "fire": Vector2.ZERO}

	var split_data = input_state_data.split(";")
	var move_input = string_to_vec2(split_data[0])
	var fire_input = string_to_vec2(split_data[1])

	return {"move": move_input, "fire": fire_input}


func set_player_position_state(player_position: Vector2) -> void:
	var position_data: String = vec2_to_string(player_position)
	playroom.set_player_state("me", "position", position_data)


func get_player_position_state(player_id: String) -> Vector2:
	var other_player_position_data: String = playroom.get_player_state(player_id, "position")
	var other_player_position = string_to_vec2(other_player_position_data)

	return other_player_position


func on_water_spawned(tile: Vector2) -> void:
	if is_singleplayer || !is_host:
		return

	var tile_data: String = vec2_to_string(tile, true)

	assert(!water_tiles.has(tile_data))
	water_tiles.append(tile_data)
	update_water_state()


func on_water_despawned(tile: Vector2) -> void:
	if is_singleplayer || !is_host:
		return

	var tile_data: String = vec2_to_string(tile, true)

	if !water_tiles.has(tile_data):
		return

	water_tiles.remove(water_tiles.find(tile_data))
	update_water_state()


func update_water_state() -> void:
	print(water_tiles)
	playroom.set_game_state("water", water_tiles.join(";"))


func on_wave_end() -> void:
	if is_singleplayer || !is_host:
		return

	killed_enemies = PoolStringArray([])
	playroom.set_game_state("spawned_enemies", "", true)
	# playroom.set_game_state("killed_enemies", "", true)

var kills: Dictionary = {}
func enemy_killed(player_id: String) -> void:
	if is_singleplayer || is_host:
		if !kills.has(player_id):
			kills[player_id] = 0

		kills[player_id] += 1

		if is_singleplayer:
			gui.update_player_killcount("me", kills[player_id])
		else:
			if player_id == playroom.my_player_id:
				gui.update_player_killcount("me", kills[player_id])
				playroom.set_player_state("me", "kills", str(kills[player_id]))
			else:
				playroom.set_player_state(player_id, "kills", str(kills[player_id]))


func kill_player_at_position(position: Vector2) -> void:
	assert(is_host || is_singleplayer)

	for p in $Game/Players.get_children():
		if p.global_position == position:
			if is_host:
				playroom.set_player_state(p.name if p != player else "me", "dead", "1")

			p.queue_free()

			return
