extends Node2D

# signal water_spawned
# signal water_despawned

const SAND_TILE_INDEX = 0
const WATER_TILE_INDEX = 3

onready var game: Node2D = $".."
onready var tilemap: TileMap = $TileMap
onready var enemy_spawns: Node2D = $EnemySpawns
onready var water_spawns: Node2D = $WaterEnemySpawns

var all_tiles_ever_changed: PoolVector2Array = PoolVector2Array()


func _ready() -> void:
	randomize()
	var _c = game.connect("wave_end", self, "on_wave_end")


func rand_enemy_spawn() -> Node:
	if randf() < 0.5:
		return enemy_spawns.get_child(int(rand_range(0, 4)))
	else:
		return enemy_spawns.get_child(int(rand_range(4, 8)))


func rand_enemy_water_spawn(_tile: Vector2 = Vector2.ZERO) -> Node:
	return rand_enemy_spawn()
	# var rand_sand_tile: Vector2 = tile
	# var rand_water_spawn: Node2D

	# if rand_sand_tile == Vector2.ZERO:
	# 	var rand_index: int = int(rand_range(0, water_spawns.get_child_count()))
	# 	rand_water_spawn = water_spawns.get_child(rand_index)
	# 	var local_pos = tilemap.to_local(rand_water_spawn.global_position)
	# 	var pos = local_pos + (local_pos.direction_to(-Vector2(16, 16)) * 16)

	# 	rand_sand_tile = tilemap.world_to_map(pos)

	# emit_signal("water_spawned", rand_sand_tile)
	# print("spawning tile: ", rand_sand_tile)
	# tilemap.set_cell(int(rand_sand_tile.x), int(rand_sand_tile.y), WATER_TILE_INDEX)

	# if !all_tiles_ever_changed.has(rand_sand_tile):
	# 	all_tiles_ever_changed.append(rand_sand_tile)

	# return rand_water_spawn


func remove_enemy_water_spawn(_spawn_node: Node2D) -> void:
	pass
	# var local_pos = tilemap.to_local(spawn_node.global_position)
	# var pos = local_pos + (-local_pos.sign() * 0.1)
	# var tile: Vector2 = tilemap.world_to_map(pos)

	# emit_signal("water_despawned", tile)
	# print("host despawning tile: ", tile)
	# tilemap.set_cell(int(tile.x), int(tile.y), SAND_TILE_INDEX)


func remove_enemy_water_tile(_tile: Vector2) -> void:
	pass
	# print("despawning tile: ", tile)
	# tilemap.set_cell(int(tile.x), int(tile.y), SAND_TILE_INDEX)


func on_wave_end() -> void:
	pass
	# for tile in all_tiles_ever_changed:
	# 	tilemap.set_cell(int(tile.x), int(tile.y), SAND_TILE_INDEX)
