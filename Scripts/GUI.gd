extends Control

const COLORS = [
	"#FF0000", "#FFFF00", "#008000",
	"#0000FF", "#800080", "#FFA500",
	"#00FFFF", "#FF00FF"
]
const COLOR_PICKER_INCREMENT: float = 100.0 / 3.0
const PLAYROOM_BANNER_SCROLL_DURATION: int = 15
const SCALE_NORMAL: Vector2 = 1.0 * Vector2.ONE
const SCALE_HOVERED: Vector2 = 1.2  * Vector2.ONE
const LOBBY_PLAYER_SCALE_FIX_TRIES: int = 10

onready var main: Node = $".."
onready var audio: Node = $"../Audio"
onready var game: Node2D = $"../Game"
onready var map: Node2D = $"../Game/Map"

onready var settings_button: TextureButton = $Right/Container/Settings/Settings/Toggle
onready var master_button: TextureButton = $Right/Container/Settings/Master/Toggle
onready var music_button: TextureButton = $Right/Container/Settings/Music/Toggle
onready var sfx_button: Button = $Right/Container/Settings/SFX/Toggle
onready var brightness_button: TextureButton = $Right/Container/Settings/Brightness/Toggle

onready var kill_counts: VBoxContainer = $Left/Container/KillCount
onready var player_kill_count: HBoxContainer = $Left/Container/KillCount/Player1
onready var wave_count: VBoxContainer = $Left/Container/WaveCount

onready var color_picker: ColorRect = $ColorPicker

onready var playroom_gui: ColorRect = $Playroom
onready var playroom_banner: TextureRect = $Playroom/PlayroomBanner
onready var play_button: Button = $Playroom/Container/Play/Play
onready var lobby_bg_border: ColorRect = $Playroom/Container/Lobby/LobbyBGBorder
onready var lobby_other_players: BoxContainer = $Playroom/Container/Lobby/Lobby/OtherPlayers
onready var lobby_player: TextureButton = $Playroom/Container/Lobby/Lobby/Container/Player
onready var host_button: Button = $Playroom/Container/Multiplayer/Buttons/Host
onready var join_button: Button = $Playroom/Container/Multiplayer/Buttons/Join
onready var join_code_edit: LineEdit = $Playroom/Container/Multiplayer/Buttons/Join/Code

var scale_fix_tries = LOBBY_PLAYER_SCALE_FIX_TRIES

var player_color: Color
var playroom_banner_tween: SceneTreeTween
var slider_percents: Dictionary = {}


func _ready() -> void:
	var start_pos = playroom_banner.rect_position
	var final_scroll_pos = Vector2(-playroom_banner.rect_size.x, playroom_banner.rect_position.y)
	playroom_banner_tween = playroom_banner.create_tween()
	var _tp = playroom_banner_tween.tween_property(playroom_banner, "rect_position", final_scroll_pos, PLAYROOM_BANNER_SCROLL_DURATION)
	var _c = playroom_banner_tween.connect("finished", self, "on_playroom_banner_finished", [start_pos])
	playroom_banner_tween.stop()

	var custom_buttons = [
		lobby_player, join_button, host_button, play_button,
		settings_button, brightness_button, master_button, music_button, sfx_button
	]

	_c = settings_button.connect("toggled", self, "on_settings_toggled")

	for toggle_button in [brightness_button, master_button, music_button, sfx_button]:
		var container = toggle_button.get_parent()
		var slider: TextureButton = container.get_node("Slider/Button")
		var setting_name: String = container.name

		toggle_button.connect("toggled", self, "on_setting_toggled", [setting_name])
		_c = slider.connect("button_down", self, "on_slider_down", [setting_name, slider])
		_c = slider.connect("button_up", self, "on_slider_up", [setting_name, slider])

		custom_buttons.append(slider)

	for button in custom_buttons:
		button.connect("mouse_entered", self, "on_button_entered", [button])
		button.connect("mouse_exited", self, "on_button_exited", [button])

	game.visible = false
	playroom_gui.visible = false
	change_brightness(0.114286)


func on_playroom_banner_finished(start_pos: Vector2) -> void:
	playroom_banner_tween.stop()
	playroom_banner.rect_position = start_pos
	playroom_banner_tween.play()


func init_color_picker() -> void:
	$ColorPicker.visible = true
	var color_picker_grid: GridContainer = $ColorPicker/GridContainer
	var color_button: TextureButton = color_picker_grid.get_child(0)

	for v in 5:
		for c in COLORS:
			var color = Color(c)
			if v < 2:
				color.s = ((v + 1) * COLOR_PICKER_INCREMENT) / 100.0
			else:
				color.v = (100.0 - ((v - 2) * COLOR_PICKER_INCREMENT)) / 100.0

			var new_button = color_button.duplicate()
			new_button.self_modulate = color
			color_picker_grid.add_child(new_button)

			new_button.connect("mouse_entered", self, "on_button_entered", [new_button])
			new_button.connect("mouse_exited", self, "on_button_exited", [new_button])
			new_button.connect("pressed", self, "on_color_selected", [color])

	color_button.queue_free()
	color_button = null


func on_button_entered(button) -> void:
	audio.play("hover")
	button.rect_scale = SCALE_HOVERED


func on_button_exited(button) -> void:
	button.rect_scale = SCALE_NORMAL


func on_color_selected(color: Color) -> void:
	audio.play("press")
	color_picker.visible = false
	playroom_gui.visible = true

	main.set_player_color(color)

	player_color = color
	lobby_bg_border.color = color
	lobby_player.self_modulate = color
	player_kill_count.get_node("CrabIcon").modulate = color
	playroom_banner_tween.play()


func on_other_player_color_selected(player_id: String, color: Color) -> void:
	var other_player_button: TextureRect = lobby_other_players.get_node_or_null(player_id)
	var kill_count_icon: TextureRect = kill_counts.get_node_or_null(player_id + "/CrabIcon")

	if !other_player_button:
		var new_kill_count = player_kill_count.duplicate()
		new_kill_count.name = player_id
		kill_counts.add_child(new_kill_count)
		kill_count_icon = new_kill_count.get_node("CrabIcon")

		for c in lobby_other_players.get_children():
			if "AddPlayer" in c.name:
				audio.play("join")
				other_player_button = c
				c.name = player_id
				break

	other_player_button.self_modulate = color
	kill_count_icon.modulate = color



func _on_ChangeColor_mouse_entered() -> void:
	lobby_player.self_modulate = Color.white


func _on_ChangeColor_mouse_exited() -> void:
	lobby_player.self_modulate = player_color


func _on_ChangeColor_pressed() -> void:
	audio.play("press")
	color_picker.self_modulate = Color(0, 0, 0, 0.6)
	color_picker.visible = true


func on_join_code_edit_unfocused() -> void:
	join_button.text = "JOIN"
	join_code_edit.visible = false


func _on_join_code_edit_gui_input(event:InputEvent) -> void:
	if !join_code_edit.has_focus() || !(event is InputEventKey):
		return

	var key = OS.get_scancode_string(event.physical_scancode)
	if !(key == "Escape" || key == "Enter"):
		return
	
	join_code_edit.release_focus()

	if key == "Enter" && join_code_edit.text.length() == 4:
		audio.play("press")
		host_button.disabled = true
		play_button.disabled = true
		join_button.disabled = true
		main.join_room(join_code_edit.text.to_upper())


func on_join_pressed() -> void:
	audio.play("hover")
	join_button.text = ""
	join_code_edit.visible = true
	join_code_edit.grab_focus()
	join_code_edit.select_all()


func on_host_pressed() -> void:
	audio.play("press")
	join_button.disabled = true
	main.join_room()
	print("GUI:", main.playroom.my_player_id)
	var code = main.get_room_code()
	if !code:
		$OneSecondTimer.start()
	else:
		audio.play("join")
		host_button.text = code


func _on_PlayButton_pressed() -> void:
	audio.play("press")
	playroom_gui.visible = false
	playroom_gui.queue_free()
	color_picker.queue_free()
	game.visible = true
	audio.play_game()
	main.play_game()


func _on_OneSecondTimer_timeout() -> void:
	var code = main.get_room_code()
	if !code:
		$OneSecondTimer.start()
	else:
		audio.play("join")
		host_button.text = code


func change_brightness(percent: float) -> void:
	var map_brightness: float = 0.6 + (0.4 * percent)
	var gui_brightness: float = 0.1 + (0.9 * percent)

	map.modulate = Color(map_brightness, map_brightness, map_brightness)
	$Right.modulate = Color(gui_brightness, gui_brightness, gui_brightness)

func on_settings_toggled(button_pressed: bool) -> void:
	audio.play("hover")

	for setting in [brightness_button, master_button, music_button, sfx_button]:
		setting.get_parent().visible = button_pressed

func on_setting_toggled(button_pressed: bool, setting_name: String) -> void:
	audio.play("hover")

	if setting_name.to_lower() == "brightness":
		var brightness: float = 0.0 if button_pressed else 1.0

		change_brightness(brightness)
		return

	audio.toggle(setting_name, button_pressed)

	if setting_name == "SFX":
		var off_rect: ColorRect = sfx_button.get_child(0).get_child(0)
		off_rect.visible = !off_rect.visible

func on_slider_down(setting_name: String, slider: TextureButton) -> void:
	if slider.is_connected("gui_input", self, "on_slider_input"):
		return
	
	var _c = slider.connect("gui_input", self, "on_slider_input", [setting_name, slider])

func on_slider_up(_setting_name: String, slider: TextureButton) -> void:
	if !slider.is_connected("gui_input", self, "on_slider_input"):
		return
	
	audio.play("hover")
	slider.disconnect("gui_input", self, "on_slider_input")

func on_slider_input(event: InputEvent, setting_name: String, slider: TextureButton) -> void:
	if !("position" in event.as_text().to_lower()):
		return

	var slider_value: ColorRect = slider.get_child(0)
	var x_pos: float = clamp(round(event.position.x), 3, slider.rect_size.x - 4)
	var percent: float = (x_pos - 3) / ((slider.rect_size.x - 4) - 3)

	if setting_name in slider_percents.keys() && slider_percents[setting_name] == percent:
		return

	slider_percents[setting_name] = percent
	slider_value.rect_position = Vector2(x_pos, slider_value.rect_position.y)

	if slider.get_parent().get_parent() == brightness_button.get_parent():
		change_brightness(percent)
	else:
		audio.volume(setting_name, percent)


func update_player_killcount(player_id: String, kill_count: int) -> void:
	var kills_label: Label = player_kill_count.get_node("Kills") if player_id == "me" else kill_counts.get_node(player_id + "/Kills")
	kills_label.text = str(kill_count)
