extends KinematicBody2D

const PROJECTILE = preload("res://Scenes/Projectile.tscn")

onready var main: Node = $"../../.."
onready var game: Node2D = $"../.."
onready var projectiles_node: Node2D = $"../../Projectiles"
onready var attack_timer: Timer = $AttackTimer
onready var joysticks2D: Node2D = $"../../TouchJoysticks"
onready var sfx_fire: AudioStreamPlayer2D = $Aim/Fire
onready var aim_node: Node2D = $Aim

var touch_joysticks: Dictionary = {
	"left": {"index": -1, "vector": Vector2.ZERO},
	"right": {"index": -1, "vector": Vector2.ZERO}
}
var speed = 2000.0
var fire_rate = 0.125
var firing = false
var viewport_center: Vector2
var current_device: String

var fire_joy_direction: Vector2 = Vector2.ZERO
var move_joy_direction: Vector2 = Vector2.ZERO

var move_input: Vector2 = Vector2.ZERO
var fire_input: Vector2 = Vector2.ZERO

var was_touch := false


func _ready() -> void:
	set_process(false)
	for c in joysticks2D.get_children():
		init_joystick_transparency_tween(c)

	viewport_center = get_viewport_rect().size / 2
	attack_timer.wait_time = fire_rate


func _process(_delta: float) -> void:
	main.set_player_input_state(move_input, fire_input)
	main.set_player_position_state(global_position)


func _physics_process(delta: float) -> void:
	var input = move_joy_direction if move_joy_direction != Vector2.ZERO else touch_joysticks.left.vector
	if input == Vector2.ZERO:
		input = Input.get_vector("left", "right", "up", "down", 0.1)
	
	move_input = input
	
	var _v = move_and_slide(input * delta * speed)


func _unhandled_input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		return

	fire_joy_direction = Input.get_vector("fire_joy_left","fire_joy_right", "fire_joy_up", "fire_joy_down", 0.1)
	move_joy_direction = Input.get_vector("move_joy_left","move_joy_right", "move_joy_up", "move_joy_down", 0.1)

	if move_joy_direction != Vector2.ZERO:
		move_joy_direction = move_joy_direction.normalized()

	if event is InputEventGesture || event is InputEventPanGesture || event is InputEventMagnifyGesture || event is InputEventScreenTouch || event is InputEventScreenDrag:
		current_device = "Touch"
		was_touch = true

		var joystick_key: String
		if event.is_pressed():
			# is free index available
			joystick_key = "left" if event.position.x <= viewport_center.x else "right"
			if touch_joysticks[joystick_key].index != -1:
				return
		else:
			for key in touch_joysticks.keys():
				if touch_joysticks[key].index == event.index:
					joystick_key = key
					break
			if !joystick_key || joystick_key == "":
				return

		var joystick_sprite = touch_joysticks[joystick_key].sprite
		var joystick_tween = touch_joysticks[joystick_key].tween

		if event is InputEventScreenTouch:
			if event.is_pressed():
				touch_joysticks[joystick_key].index = event.index

				joystick_sprite.position = event.position
				joystick_tween.stop()
				joystick_sprite.modulate = Color.white
				touch_joysticks[joystick_key].vector = Vector2.DOWN
			else:
				touch_joysticks[joystick_key].index = -1
				touch_joysticks[joystick_key].vector = Vector2.ZERO

				joystick_tween.play()

			if joystick_key == "right":
				firing = event.is_pressed()

				if firing && attack_timer.is_stopped():
					fire()
		else:
			var dir = joystick_sprite.position.direction_to(event.position)
			if dir == Vector2.ZERO:
				dir = Vector2.DOWN
			touch_joysticks[joystick_key].vector = dir
			joystick_sprite.get_child(0).position = dir * 10

		return
	elif event is InputEventKey || event is InputEventMouse:
		current_device = "M&K"
	elif (event is InputEventJoypadButton || event is InputEventJoypadMotion) && (fire_joy_direction != Vector2.ZERO || move_joy_direction != Vector2.ZERO):
		current_device = "Joypad"

	if current_device == "Touch" || was_touch:
		was_touch = false
		return

	if event.is_action_pressed("quit"):
		get_tree().quit()

	if event.is_action_pressed("fire") || fire_joy_direction != Vector2.ZERO:
		firing = true

		if attack_timer.is_stopped():
			fire()
	elif event.is_action_released("fire") || (!Input.is_action_pressed("fire") && fire_joy_direction == Vector2.ZERO):
		firing = false
		fire_input = Vector2.ZERO


func fire() -> void:
	var fire_direction = fire_joy_direction if fire_joy_direction != Vector2.ZERO else touch_joysticks.right.vector
	if fire_direction == Vector2.ZERO && current_device == "M&K":
		fire_direction = get_local_mouse_position()
	if fire_direction != Vector2.ZERO:
		fire_direction = fire_direction.normalized()

	fire_input = fire_direction

	var new_projectile = PROJECTILE.instance()
	new_projectile.player_id_from = main.playroom.my_player_id
	new_projectile.modulate = modulate
	new_projectile.damage *= game.player_damage_multiplier
	new_projectile.velocity = fire_direction
	projectiles_node.add_child(new_projectile)
	new_projectile.global_position = global_position

	aim_node.look_at(global_position + fire_input)
	sfx_fire.play()
	attack_timer.start()


func _on_AttackTimer_timeout() -> void:
	if firing:
		fire()
	else:
		fire_input = Vector2.ZERO


func init_joystick_transparency_tween(joystick_gui: Sprite) -> void:
	var tween = joystick_gui.create_tween()
	tween.tween_property(joystick_gui, "modulate", Color(1, 1, 1, 0), 1.0).from(Color(1, 1, 1, 1))

	tween.connect("finished", self, "_on_joystick_tween_finished", [tween])

	touch_joysticks[joystick_gui.name.to_lower()]["sprite"] = joystick_gui
	touch_joysticks[joystick_gui.name.to_lower()]["tween"] = tween


func _on_joystick_tween_finished(tween: SceneTreeTween) -> void:
	tween.stop()

