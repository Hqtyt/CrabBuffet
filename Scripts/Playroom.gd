extends Node

const ENEMY = preload("res://Scenes/Enemy.tscn")
const OTHER_PLAYER_SCENE = preload("res://Scenes/OtherPlayer.tscn")
const GAME_STATES = {
	"playing": "0",
	"killed_enemies": "",
	"spawned_enemies": "",
	"spawns": "",
	"water": ""
}
const PLAYER_STATES = {
	"character": null, "player": null,
	"color": "#ffffff",
	"input": "0.0,0.0;0.0,0.0",
	"position": "144.0,80.0",
	"kills": "0",
	"dead": "0"
}

onready var main: Node = $".."
onready var gui: Control = $"../GUI"
onready var map: Node2D = $"../Game/Map"
onready var player_spawn_nodes: Node2D = $"../Game/Map/PlayerSpawns"
onready var players_node: Node2D = $"../Game/Players"
onready var player: KinematicBody2D = $"../Game/Players/Player"
onready var enemies_node: Node2D = $"../Game/Enemies"

var Playroom: JavaScriptObject = JavaScript.get_interface("Playroom")

var game_state: Dictionary = {}
var player_states: Dictionary = {}
var my_player_id: String


func _ready() -> void:
	set_process(false)
	set_physics_process(false)


func get_game_state(state: String) -> String:
	assert(state in GAME_STATES.keys())

	var data: String = Playroom.getState(state)

	if data != game_state[state]:
		game_state[state] = data

	return data


func get_player_state(player_id: String, state: String) -> String:
	assert(player_id in player_states.keys() && state in player_states[player_id].keys())

	var data: String = player_states[player_id].player.getState(state)

	if data != player_states[player_id][state]:
		player_states[player_id][state] = data

	return data


func set_game_state(state: String, data: String, reliable: bool = false) -> void:
	assert(state in game_state.keys())

	Playroom.setState(state, data, reliable)
	game_state[state] = data


func set_player_state(player_id: String, state: String, data: String, reliable: bool = false) -> void:
	if player_id == "me":
		player_id = Playroom.myPlayer().id

	assert(player_id in player_states.keys() && state in player_states[player_id].keys())

	player_states[player_id].player.setState(state, data, reliable)
	player_states[player_id][state] = data


func play_game() -> void:
	var spawn_data: PoolStringArray = PoolStringArray([])
	var i: int = 1
	for player_id in player_states.keys():
		spawn_data.append(player_id + ":" + str(i))
		i += 1

	set_game_state("spawns", spawn_data.join(";"), true)
	set_game_state("playing", "1", true)
	spawn_players()


func spawn_players():
	player.set_process(true)

	var spawn_data: PoolStringArray = get_game_state("spawns").split(";", false)

	for spawn in spawn_data:
		var player_spawn_data = spawn.split(":", false)
		var player_id = player_spawn_data[0]
		var player_spawn = player_spawn_data[1]
		var spawn_node = player_spawn_nodes.get_node(player_spawn)

		player_states[player_id].character.global_position = spawn_node.global_position


func get_room_code():
	return Playroom.getRoomCode()


func set_player_color(color: Color) -> void:
	set_player_state("me", "color", color.to_html(false), true)


func update_colors() -> void:
	for player_id in player_states.keys():
		if player_id == Playroom.myPlayer().id:
			continue

		var old_color = player_states[player_id].color
		var new_color = get_player_state(player_id, "color")

		if new_color != old_color:
			player_states[player_id].color = new_color
			player_states[player_id].character.modulate = Color(new_color)
			gui.on_other_player_color_selected(player_id, Color(new_color))


func get_spawned_enemies() -> Array:
	assert(!Playroom.isHost())

	var data: String = get_game_state("spawned_enemies")

	var spawned_enemies: Array = []
	var enemies_data: PoolStringArray = data.split(";", false)

	for enemy_data in enemies_data:
		var modulate_i = enemy_data.find("m")
		var sleeping_i = enemy_data.find("s")
		var position_i = enemy_data.find("(")

		var enemy_name: String = enemy_data.substr(0, modulate_i)
		var enemy_modulate_a: float = float(enemy_data.substr(modulate_i + 1, sleeping_i - modulate_i - 1))
		var enemy_sleeping: bool = enemy_data.substr(sleeping_i + 1, position_i - sleeping_i - 1) == "1"
		var pos_data: PoolRealArray = enemy_data.substr(position_i + 1, enemy_data.find(")") - position_i - 1).split_floats(",")
		var enemy_position: Vector2 = Vector2(pos_data[0], pos_data[1])

		spawned_enemies.append({
			"name": enemy_name,
			"global_position": enemy_position,
			"modulate_a": enemy_modulate_a,
			"sleeping": enemy_sleeping
		})

	return spawned_enemies


func set_spawned_enemies() -> void:
	var array_data: Array = []

	for e in enemies_node.get_children():
		if !(e is KinematicBody2D):
			continue
		
		var enemy_data: String = e.name
		var sleeping = enemies_node.sleeping_enemies.has(e.name) && enemies_node.sleeping_enemies[e.name].sleeping
		enemy_data += "m"+ str(e.modulate.a)
		enemy_data += "s" + ("1" if sleeping else "0")
		var rounded_position: Vector2 = (e.global_position * 1000.0).round() / 1000
		enemy_data += "(" + ("%.3f" % rounded_position.x) + "," + ("%.3f" % rounded_position.y) + ")"
		array_data.append(enemy_data)

	var data: String = PoolStringArray(array_data).join(";")

	set_game_state("spawned_enemies", data, true)


func update_enemies() -> void:
	# var my_killed_enemies: PoolStringArray = main.killed_enemies
	# var killed_enemies_data: String = get_game_state("killed_enemies")
	# var killed_enemies_names: PoolStringArray = killed_enemies_data.split(",", false)
	# var updated_killed_enemies_data: String

	# for enemy_name in killed_enemies_names:
	# 	var living_enemy = enemies_node.get_node_or_null(enemy_name)

	# 	if living_enemy && !living_enemy.is_queued_for_deletion():
	# 		living_enemy.queue_free()
		
	# 	if enemy_name in my_killed_enemies:
	# 		my_killed_enemies.remove(my_killed_enemies.find(enemy_name))

	# if my_killed_enemies.size() > 0:
	# 	killed_enemies_names.append_array(my_killed_enemies)
	# 	updated_killed_enemies_data = killed_enemies_names.join(",")
	# 	set_game_state("killed_enemies", updated_killed_enemies_data, true)

	if Playroom.isHost():
		set_spawned_enemies()
		return

	var spawned_enemies: Array = get_spawned_enemies()
	var spawned_enemies_names: PoolStringArray = PoolStringArray([])

	for enemy_data in spawned_enemies:
		spawned_enemies_names.append(enemy_data.name)
		var living = enemies_node.get_node_or_null(enemy_data.name)
		if !living:
			var new_enemy = ENEMY.instance()
			new_enemy.name = enemy_data.name
			enemies_node.add_child(new_enemy)
			new_enemy.global_position = enemy_data.global_position
			continue

		if living.is_queued_for_deletion():
			continue

		if living.modulate.a > enemy_data.modulate_a:
			living.modulate.a = enemy_data.modulate_a

		if living.global_position.distance_to(enemy_data.global_position) > 10.0:
			living.global_position = enemy_data.global_position
		
		if !enemies_node.sleeping_enemies.has(living.name):
			enemies_node.sleeping_enemies[living.name] = {"sleeping": enemy_data.sleeping}
		else:
			enemies_node.sleeping_enemies[living.name].sleeping = enemy_data.sleeping


	for enemy in enemies_node.get_children():
		if !(enemy is KinematicBody2D) || enemy.is_queued_for_deletion():
			continue

		if !(enemy.name in spawned_enemies_names):
			if enemies_node.sleeping_enemies.has(enemy.name):
				enemies_node.sleeping_enemies.erase(enemy.name)

			enemy.queue_free()


var player_kills: Dictionary = {}
func update_kills() -> void:
	for player_id in player_states.keys():
		var kills: int = int(get_player_state(player_id, "kills"))

		if player_kills.has(player_id) && player_kills[player_id] == kills:
			continue

		player_kills[player_id] = kills
		if player_id == my_player_id:
			gui.update_player_killcount("me", kills)
		else:
			gui.update_player_killcount(player_id, kills)


var water_tiles: PoolStringArray = PoolStringArray()
func update_water_tiles() -> void:
	var data: String = get_game_state("water")
	var split_data: PoolStringArray = PoolStringArray([data])

	if ";" in data:
		split_data = data.split(";", false)

	for tile_data in split_data:
		if tile_data == "" || !("," in tile_data):
			continue

		if !water_tiles.has(tile_data):
			map.rand_enemy_water_spawn(main.string_to_vec2(tile_data, true))
			water_tiles.append(str(tile_data))
	
	var main_water: PoolStringArray = PoolStringArray(Array(water_tiles))
	for tile_data in main_water:
		if !(tile_data in split_data):
			map.remove_enemy_water_tile(main.string_to_vec2(tile_data, true))
			water_tiles.remove(water_tiles.find(tile_data))
	print(water_tiles)


func update_players() -> void:
	for player_id in player_states.keys():
		if player_states[player_id].dead == "1":
			continue

		var player_char = player_states[player_id].character
		var exists = player_char != null && player_char is KinematicBody2D && player_char.get_parent() && player_char.is_inside_tree() && !player_char.is_queued_for_deletion()

		if exists && get_player_state(player_id, "dead") == "1":
			player_states[player_id].character.queue_free()
			player_states[player_id].character = null


func _process(_delta: float) -> void:
	if main.is_singleplayer:
		return
	elif !main.playing:
		update_colors()

		if !Playroom.isHost() && get_game_state("playing") == "1" && get_game_state("spawns") != "":
			main.playing = true
			spawn_players()
			gui._on_PlayButton_pressed()
	else: # if during wave
		update_enemies()
		update_kills()
		update_players()

		# if !main.is_host:
		# 	update_water_tiles()



func init_playroom(room_code: String = "") -> void:
	var initOptions = JavaScript.create_object("Object")

	var default_game_states = JavaScript.create_object("Object")
	var game_states_template = GAME_STATES.duplicate()
	default_game_states.playing = game_states_template.playing
	# default_game_states.killed_enemies = game_states_template.killed_enemies
	default_game_states.spawned_enemies = game_states_template.spawned_enemies
	default_game_states.spawns = game_states_template.spawns
	default_game_states.water = game_states_template.water
	game_state = game_states_template

	var default_player_states = JavaScript.create_object("Object")
	var player_states_template = PLAYER_STATES.duplicate()
	default_player_states.color = player_states_template.color
	default_player_states.input = player_states_template.input
	default_player_states.position = player_states_template.position

	initOptions.defaultStates = default_game_states
	initOptions.defaultPlayerStates = default_player_states

	initOptions.skipLobby = true
	initOptions.maxPlayersPerRoom = 4
	if room_code != "":
		initOptions.roomCode = room_code

	Playroom.insertCoin(initOptions, bridgeToJS("onInsertCoin"))


# Keep a reference to the callback so it doesn't get garbage collected
var jsBridgeReferences = []
func bridgeToJS(cb):
	var jsCallback = JavaScript.create_callback(self, cb)
	jsBridgeReferences.push_back(jsCallback)
	return jsCallback


# Called when the host has started the game
func onInsertCoin(_args):
	Playroom.onPlayerJoin(bridgeToJS("onPlayerJoin"))

	var new_player_states = PLAYER_STATES.duplicate()
	new_player_states.character = player
	new_player_states.player = Playroom.myPlayer()
	new_player_states.color = player.modulate.to_html(false)
	my_player_id = Playroom.myPlayer().id
	player_states[my_player_id] = new_player_states

	for state in player_states[my_player_id].keys():
		if state == "player" || state == "character":
			continue

		set_player_state(my_player_id, state, player_states[my_player_id][state], true)

	main.is_host = Playroom.isHost()
	main.is_singleplayer = false
	set_process(true)


# Called when a new player joins the game
func onPlayerJoin(args):
	var other_player = args[0];

	if other_player.id == Playroom.myPlayer().id:
		return

	var other_player_states = PLAYER_STATES.duplicate()
	other_player_states.player = other_player
	other_player_states.character = OTHER_PLAYER_SCENE.instance()
	player_states[other_player.id] = other_player_states
	player_states[other_player.id].color = get_player_state(other_player.id, "color")

	other_player_states.character.name = other_player.id
	players_node.add_child(other_player_states.character)
	other_player_states.character.global_position = Vector2(144, 80)
	other_player_states.character.modulate = player_states[other_player.id].color

	gui.on_other_player_color_selected(other_player.id, Color(player_states[other_player.id].color))

	# Listen to onQuit event
	other_player.onQuit(bridgeToJS("onPlayerQuit"))


func onPlayerQuit(args):
	var state = args[0];

	print("QUITING: ", state.id)
	if player_states[state.id].character != null:
		player_states[state.id].character.queue_free()

	var _v = player_states.erase(state.id)
