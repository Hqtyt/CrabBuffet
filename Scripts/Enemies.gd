extends Node2D

const ENEMY = preload("res://Scenes/Enemy.tscn")
const CHECKS_UNTIL_SLEEP = 30
const MAX_ENEMIES_AT_ONCE = 15

onready var main: Node = $"../.."
onready var audio: Node = $"../../Audio"
onready var game: Node = $".."
onready var map: Node2D = $"../Map"
onready var enemies_timer: Timer = $Timers/EnemiesTimer
onready var enemies_water_timer: Timer = $Timers/EnemiesWaterTimer
onready var players: Node2D = $"../Players"

var speed: float = 1500.0
var enemy_wave: int = 1
# enemies to spawn during wave. Scales
var enemy_wave_ratio: int = 15
# enemies to spawn in a row from a single spawn point. Scales
var enemy_spawn_ratio: int = 5
# enemies to spawn between water tiles spawning. Scales
var enemy_water_ratio: int = 10

var enemy_spawn_rate: float
var enemies_per_spawn: int
var enemies_to_spawn_this_wave: int
var enemies_to_spawn_at_current_spawn: int
var enemies_to_spawn_at_current_water_spawn: int
var spawns_until_water_tile: int

var current_enemy_spawn: Node
var current_enemy_water_spawn: Node

var sleeping_enemies: Dictionary = {}


func _ready() -> void:
	var _c = game.connect("wave_start", self, "on_wave_start")
	_c = enemies_timer.connect("timeout", self, "_on_spawn_timeout")
	_c = enemies_water_timer.connect("timeout", self, "_on_spawn_timeout", [true])


func on_wave_start(_new_wave_plan: Array = []):
	var player_amount: int = players.get_child_count()

	enemies_to_spawn_this_wave = enemy_wave * (enemy_wave_ratio * player_amount)
	enemies_per_spawn = enemy_spawn_ratio * player_amount
	enemies_to_spawn_at_current_spawn = enemies_per_spawn
	enemies_to_spawn_at_current_water_spawn = enemies_per_spawn
	spawns_until_water_tile = int(float(enemy_water_ratio) / enemies_per_spawn)

	enemy_spawn_rate = 0.4 / player_amount
	enemies_timer.wait_time = enemy_spawn_rate
	enemies_timer.start(enemy_spawn_rate)


func on_wave_end():
	print("wave end: ", enemy_wave)
	enemies_timer.stop()
	enemy_wave += 1

	if current_enemy_water_spawn:
		map.remove_enemy_water_spawn(current_enemy_water_spawn)

	current_enemy_spawn = null

	game.end_wave()


func _physics_process(delta: float) -> void:
	for enemy in get_children():
		if !(enemy is KinematicBody2D) || enemy.is_queued_for_deletion():
			continue

		var min_distance: float = 999999.0
		var closest_player_position: Vector2
		for player in players.get_children():
			var distance = enemy.global_position.distance_to(player.global_position)

			if distance < min_distance:
				min_distance = distance
				closest_player_position = player.global_position

		var velocity := Vector2.ZERO
		var sleeping = sleeping_enemies.has(enemy.name) && sleeping_enemies[enemy.name].sleeping

		if !sleeping && min_distance > 10.0:
			var closest_player_dir = enemy.global_position.direction_to(closest_player_position)
			velocity = closest_player_dir * speed * delta

			enemy.move_and_slide(velocity)


		if main.is_singleplayer || main.is_host:
			var distance_moved: float = velocity.length() / (1.0 / delta)
			update_sleep(enemy, closest_player_position, distance_moved)


func update_sleep(enemy: KinematicBody2D, player_position: Vector2, distance_moved: float):
	if !sleeping_enemies.has(enemy.name):
		sleeping_enemies[enemy.name] = {
			"sleeping": false,
			"position": enemy.global_position,
			"player_position": player_position,
			"checks_until_sleep": CHECKS_UNTIL_SLEEP
		}

		return

	if sleeping_enemies[enemy.name].sleeping:
		if sleeping_enemies[enemy.name].player_position.distance_to(player_position) > 10.0:
			sleeping_enemies[enemy.name].sleeping = false
			sleeping_enemies[enemy.name].checks_until_sleep = CHECKS_UNTIL_SLEEP
			sleeping_enemies[enemy.name].player_position = player_position
			# enemy.get_node("CrabColl").modulate = Color.black

		return

	if sleeping_enemies[enemy.name].checks_until_sleep <= 0:
		sleeping_enemies[enemy.name].sleeping = true
		if enemy.global_position.distance_to(player_position) < 10.0:
			main.kill_player_at_position(player_position)
		# enemy.get_node("CrabColl").modulate = Color.darkgray
	elif distance_moved <= 0 || sleeping_enemies[enemy.name].position.distance_to(enemy.global_position) < distance_moved - 0.01:
		sleeping_enemies[enemy.name].checks_until_sleep -= 1
	elif sleeping_enemies[enemy.name].checks_until_sleep != CHECKS_UNTIL_SLEEP:
		sleeping_enemies[enemy.name].checks_until_sleep = CHECKS_UNTIL_SLEEP

	sleeping_enemies[enemy.name].position = enemy.global_position
	sleeping_enemies[enemy.name].player_position = player_position


func spawn_enemy(spawn_node: Node2D) -> void:
	var new_enemy = ENEMY.instance()
	var spawn_id = spawn_node.get_parent().name.to_lower().substr(0, 1)
	var new_name = str(enemy_wave) + spawn_id + str(enemies_to_spawn_this_wave)

	new_enemy.name = new_name
	add_child(new_enemy)
	new_enemy.global_position = spawn_node.global_position

	enemies_to_spawn_this_wave -= 1


func _on_spawn_timeout(is_water_spawn: bool = false) -> void:
	assert(main.is_singleplayer || main.is_host)

	var current_spawn: Node2D = current_enemy_spawn if !is_water_spawn else current_enemy_water_spawn

	if is_water_spawn:
		enemies_water_timer.start(enemy_spawn_rate)
	else:
		enemies_timer.start(enemy_spawn_rate)

	if enemies_to_spawn_this_wave <= 0:
		if current_enemy_water_spawn:
			map.remove_enemy_water_spawn(current_enemy_water_spawn)
			enemies_water_timer.stop()

		return

	if get_child_count() >= MAX_ENEMIES_AT_ONCE:
		return

	if is_water_spawn && enemies_to_spawn_at_current_water_spawn <= 0:
		map.remove_enemy_water_spawn(current_spawn)
		enemies_to_spawn_at_current_water_spawn = enemies_per_spawn
		current_enemy_water_spawn = null
		enemies_water_timer.stop()
		return
	elif !is_water_spawn && (!current_spawn || enemies_to_spawn_at_current_spawn <= 0):
		enemies_to_spawn_at_current_spawn = enemies_per_spawn

		if spawns_until_water_tile <= 0:
			spawns_until_water_tile = int(float(enemy_water_ratio) / enemies_per_spawn)
			if current_enemy_water_spawn:
				map.remove_enemy_water_spawn(current_enemy_water_spawn)
			current_enemy_water_spawn = map.rand_enemy_water_spawn()
			enemies_water_timer.start(enemy_spawn_rate * 4)
		else:
			spawns_until_water_tile -= 1
		
		current_enemy_spawn = map.rand_enemy_spawn()
		current_spawn = current_enemy_spawn

	if is_water_spawn:
		enemies_to_spawn_at_current_water_spawn -= 1
	else:
		enemies_to_spawn_at_current_spawn -= 1

	spawn_enemy(current_spawn)


func _on_child_exiting_tree(node:Node) -> void:
	if node is KinematicBody2D:
		audio.play2D("pop", node.global_position)

		if sleeping_enemies.has(node.name):
			var _e = sleeping_enemies.erase(node.name)

	if enemies_to_spawn_this_wave > 0 || !game.wave_cd_timer.is_stopped():
		return

	for enemy in get_children():
		if enemy is KinematicBody2D && !enemy.is_queued_for_deletion():
			return

	if main.is_singleplayer || main.is_host:
		on_wave_end()
