extends Node

const SFX_DAMAGE = preload("res://Scenes/Sounds/SFX_Damage.tscn")
const SFX_POP = preload("res://Scenes/Sounds/SFX_Pop.tscn")
const MIN_VOLUME = -50.0
const MAX_VOLUME = 5.0

onready var lobby_music: AudioStreamPlayer = $LobbyMusic
onready var game_music: AudioStreamPlayer = $GameMusic
onready var sfx_game_over: AudioStreamPlayer = $SFX_GameOver
onready var sfx_joined: AudioStreamPlayer = $PlayerJoined
onready var sfx_button_hover: AudioStreamPlayer = $ButtonHover
onready var sfx_button_press: AudioStreamPlayer = $ButtonPress
onready var game_sounds_node: Node2D = $"../Game/Sounds"

onready var music_bus = AudioServer.get_bus_index("Music")
onready var master_bus = AudioServer.get_bus_index("Master")


func _ready() -> void:
	for bus_name in ["Master", "Music", "SFX"]:
		volume(bus_name, 0.8)

	game_music.volume_db = -30
	game_music.stop()
	lobby_music.volume_db = -50
	lobby_music.stop()


func play_game() -> void:
	var transition_tween = lobby_music.create_tween()
	transition_tween.tween_property(lobby_music, "volume_db", -50.0, 10.0)
	transition_tween.connect("finished", self, "on_transition_finished")
	var transition_game_tween = game_music.create_tween()
	transition_game_tween.tween_property(game_music, "volume_db", -5.0, 10.0)
	game_music.play()


func on_transition_finished() -> void:
	lobby_music.stop()


func play2D(sfx_name: String, audio_position: Vector2) -> void:
	var new_sound

	if "pop" in sfx_name.to_lower():
		new_sound = SFX_POP.instance()
	elif "damage" in sfx_name.to_lower():
		new_sound = SFX_DAMAGE.instance()

	new_sound.position = audio_position
	game_sounds_node.add_child(new_sound)


func play(sfx_name: String) -> void:
	var sfx_l = sfx_name.to_lower()

	for sfx in [sfx_game_over, sfx_joined, sfx_button_hover, sfx_button_press]:
		if sfx_l in sfx.name.to_lower():
			sfx.play()
			break


func toggle(bus_name: String, enabled: bool) -> void:
	var bus_idx: int = AudioServer.get_bus_index(bus_name)
	AudioServer.set_bus_mute(bus_idx, enabled)


func volume(bus_name: String, volume_percent: float) -> void:
	var bus_idx: int = AudioServer.get_bus_index(bus_name)
	var volume: float = MIN_VOLUME + (abs(MAX_VOLUME - MIN_VOLUME) * volume_percent)
	if volume == MIN_VOLUME:
		volume = -80.0

	AudioServer.set_bus_volume_db(bus_idx, volume)


# func on_game_over() -> void:
# 	game_over.play()


func _on_InitialTimer_timeout() -> void:
	var intro_tween = lobby_music.create_tween()
	intro_tween.tween_property(lobby_music, "volume_db", -5.0, 10.0)
	lobby_music.play()
